FROM alpine as builder

WORKDIR /app
RUN apk add --no-cache yarn

COPY package.json yarn.lock /app/
RUN yarn install

COPY . /app
RUN yarn generate

FROM nginx

COPY --from=builder /app/dist /usr/share/nginx/html
